# AsignacionGit

##### ¿Que es Git?
Es un sistema o software de control de versiones (VCS) diseñado por Linus Torvalds  pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Permite registrar todo el historial de cambio de un proyecto determinado y coordinar el trabajo que varias personas realizan sobre archivos compartidos.

##### ¿Que es GitLab?

GitLab nació como un sistema de alojamiento de repositorios Git, es decir, un hosting para proyectos gestionados por el sistema de versiones Git. Fue  escrito por los programadores ucranianos Dmitriy Zaporozhets y Valery Sizov en el lenguaje de programación Ruby  con algunas partes reescritas posteriormente en Go

Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.

##### ¿Que es GitHub?

GitHub es un sitio web y un servicio en la nube que ayuda a los desarrolladores a almacenar y administrar su código, al igual que llevar un registro y control de cualquier cambio sobre este código. Esencialmente, hace que sea más fácil para individuos y equipos usar Git como la versión de control y colaboración.

Fue desarrollado por Chris Wanstrath, P. J. Hyett, Tom Preston-Werner y Scott Chacon usando Ruby on Rails, y empezó en 2008. Aunque la compañía, Github, Inc, existía desde 2007.

##### ¿Qué es Markdown?

Markdown es un lenguaje de marcado que facilita la aplicación de formato a un texto empleando una serie de caracteres de una forma especial. En principio, fue pensado para elaborar textos cuyo destino iba a ser la web con más rapidez y sencillez que si estuviésemos empleando directamente HTML. Y si bien ese suele ser el mejor uso que podemos darle, también podemos emplearlo para cualquier tipo de texto, independientemente de cual vaya a ser su destino.

##### Describa los Comandos Basicos de Git

- **$ git config --global user.name "[name]"**: Establece el nombre que desea esté anexado a sus transacciones de commit.

- **$ git config --global user.email "[email address]"**:  Establece el e-mail que desea esté anexado a sus transacciones de commit.

- **$ git init [project-name]**: Crea un nuevo repositorio local con el nombre especificado.

- **$ git clone [url]**: Descarga un proyecto y todo el contenido que este contenga a la PC.

*Vía HTTP:*
**$ git clone http://dominio.com/usuario/repositorio.git**

*Vía SSH:* 
**$ git clone ssh://usuario@dominio.com/repo.git**

- **$ git status**: Despliega el listado y enumera los archivos y carpetas que han sufrido cambios y los que aun no han sido agregados o confirmados mediante commit.

- **$ git log**: Muestra el historial del proyecto, son los cambios ya confirmados.

- **$ git add .** : Agrega todos los archivos que tengan cambios.

- **$ git rm .** : Si queremos eliminar los archivos que acabamos de añadir para ser guardados

- **$ git commit -m "Mensaje descriptivo de tus cambios"**: Es el comando para confirmar que se efectuarán los cambios de los archivos definidos con git add. Con -m agregas un mensaje que describa de manera clara tus cambios.

- **$ git branch**: Despliega las ramas locales existentes.

- **$ git push origin master**:  Para enviar los cambios a la rama master de un repositorio remoto.

- **$ git pull**: Descarga y combina los cambios ubicados en el repositorio remoto a tu directorio de trabajo.

- **$ git merge [nombredelarama]** : Para fusionar una rama diferente en tu rama activa.

- **$ git diff** : Para visualizar todos los conflictos luego de un git merge o fusión.

#####  Ramas

Las ramas son utilizadas para desarrollar funcionalidades aisladas unas de otras. La rama master es la rama "por defecto" cuando creas un repositorio. Crea nuevas ramas durante el desarrollo y fusiónalas a la rama principal cuando termines.

##### ¿Cuando usar git init?

Para crear un nuevo repositorio, se usa el comando git init. git init es un comando que se utiliza una sola vez durante la configuración inicial de un repositorio nuevo. Al ejecutar este comando, se creará un nuevo subdirectorio .git en tu directorio de trabajo actual. También se creará una nueva rama maestra.

##### ¿Para que se usa git clone?

El comando git clone se usa para crear una copia o clonar un repositorio remoto, es decir, trae todo lo que esta en el repositorio a la PC. Se utiliza git clone con la URL de un repositorio, Git es compatible con varios protocolos de red y sus formatos de URL correspondientes

##### ¿Cuando usar git push?

El comando git push se usa para cargar contenido del repositorio local a un repositorio remoto. El envío es la forma de transferir commits desde tu repositorio local a un repositorio remoto.

git push se suele usar para publicar y cargar cambios locales a un repositorio central. Después de modificar el repositorio local, se ejecuta un comando push para compartir las modificaciones con miembros remotos del equipo.

##### ¿Diferencias entre git push y git pull?

Git Push envía un conjunto de confirmaciones y los objetos a los que apuntan a un control remoto. En cambio el Git Pull copia un conjunto de confirmaciones y objetos referidos desde un control remoto.

##### ¿Cuando usar git add?

El comando git add añade contenido o archivos modificados del directorio de trabajo al área de ensayo (staging area o 'index') para la próxima confirmación o commit.

Indica a Git que quieres incluir actualizaciones en un archivo concreto en la próxima confirmación. Sin embargo, git add no afecta al repositorio de manera significativa: en realidad, los cambios no se registran hasta que ejecutas git commit.

##### ¿Explique que cual es la finalidad de un git commit?

El comando "commit" se usa para guardar los cambios en el repositorio local. El uso del comando "git commit" solo guarda un nuevo objeto de confirmación en el repositorio local de Git.

Sin embargo hay que tener en cuenta que se debe decirle explícitamente a Git qué cambios desea incluir en una confirmación antes de ejecutar el comando "git commit". Esto significa que un archivo no se incluirá automáticamente en la siguiente confirmación solo porque se modificó. En su lugar, debe utilizar el comando "git add" para marcar los cambios deseados para su inclusión.

##### Explique y de ejemplo de como funciona el flujo de trabajo en GIT

Un flujo de trabajo de Git es una fórmula o una recomendación acerca del uso de Git para realizar trabajo de forma uniforme y productiva. Los flujos de trabajo de Git animan a los usuarios a sacar partido a Git de forma eficaz y estable. Git ofrece a los usuarios una amplia flexibilidad de gestión de cambios. Dado que Git se centra en la flexibilidad, no existe un proceso estandarizado acerca de cómo interactuar con Git. Para garantizar que todo el equipo se encuentra en sintonía, se debe desarrollar o seleccionar un flujo de trabajo de Git. Si decidimos desarrollar un flujo propio en el equipo hay que buscar la manera de que sea efectivo, productivo y ocasione minimos problemas entre los miembros del equipo, sin embargo, una opción más viable es escoger algunos de los flujos ya existentes y documentados, tales flujos se van a mencionar y explicar su idea de como trabajar:

##### Flujo de trabajo centralizado

Este flujo de trabajo se basa en el uso de un repositorio en la nube el cual tendra una sola rama que sera **"master"** y todo el desarrollo se tendra en esta, suele ser metodo más usado por su facilidad al momento de trabajar, su funcionamiento consiste en que los desarrolladores empiezan por clonar el repositorio central. En sus propias copias locales del proyecto, editan archivos y confirman cambios, no obstante, estas nuevas confirmaciones se almacenan de forma local, completamente aisladas del repositorio central. Esto permite que los desarrolladores aplacen la sincronización con los niveles superiores hasta que realicen una pausa. Para publicar cambios en el proyecto oficial, los desarrolladores "envían" su rama master local al repositorio central.

##### Ramas de funcionalidades

La creación de ramas de funciones es una extensión lógica del flujo de trabajo centralizado. La idea principal que subyace al flujo de trabajo de rama de función es que el desarrollo de una función debe llevarse a cabo en una rama especializada, en lugar de en una rama master. Este aislamiento permite que varios desarrolladores trabajen en una función concreta sin perturbar el contenido del código base principal. También implica que la rama master no debe contener en ningún caso código erróneo, lo que supone una gran ventaja para los entornos de integración continua. 

##### Flujo de trabajo de Gitflow

El flujo de trabajo de Gitflow se publicó por primera vez en una entrada de blog de 2010 muy reconocida de Vincent Driessen en nvie. El flujo de trabajo de Gitflow define un modelo de creación de ramas estricto diseñado con la publicación del proyecto como fundamento. Este flujo de trabajo no añade ningún concepto o comando nuevo, solo los que se necesitan para el flujo de trabajo de rama de función. En su lugar, asigna funciones muy específicas a las distintas ramas y define cómo y cuándo deben realizarse las interacciones. 


##### Flujo de trabajo de bifurcación

El flujo de trabajo de bifurcación es muy diferente de los otros flujos de trabajo, en lugar de usar un único repositorio en servidor para que actúe como código base central, le proporciona a cada desarrollador un repositorio en servidor. Esto quiere decir que cada contribuyente tiene dos repositorios de Git: uno local y privado y otro público y en servidor.

